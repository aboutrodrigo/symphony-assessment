# Automated Test Instructions

This repository contains automated tests for API and UI using Cypress. Follow the instructions below to run the tests locally and on CI.

## Prerequisites

- Node.js and npm installed on your machine. Latest version please.

## Setup

1. Clone the Repository:
   - Clone the repository containing the automated tests to your local machine.

2. Install Dependencies:
   - Navigate to the project root directory.
   - Run the following command to install the project dependencies:
     ```
     npm install
     ```

## Running the tests over Cypress

To run the API tests locally, use the following command:   

 ```
     npm run cy:open
```

It's going to open and run the cypress application, then select the tests to run and check the results

## Running API Tests

To run the API tests locally, use the following command:

 ```
     npm run cy:api
```

This command will execute the API test suite using Cypress, and the test results will be displayed in the terminal/console.

## Running UI Tests

To run the UI tests locally, use the following command:

 ```
     npm run cy:api
```

This command will execute the UI test suite using Cypress, and the test results will be displayed in the terminal/console.


## Running All Tests

To run both the API and UI tests together, use the following command:

 ```
     npm run cy:all
```

This command will execute both the API and UI test suites using Cypress, and the test results will be displayed in the terminal/console.

## Creating the Allure Reporting

To generate the Allure Reporting, run

 ```
     npm run allure:report
```

and then, to open the report in the browser, just run in the terminal

 ```
     allure open
```
