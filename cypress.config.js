const { defineConfig } = require("cypress");
// import allureWriter from "@shelex/cypress-allure-plugin/writer";
const allureWriter = require('@shelex/cypress-allure-plugin/writer');
// import allureWriter from "@shelex/cypress-allure-plugin/writer";

module.exports = defineConfig({
  e2e: {
    chromeWebSecurity: false,
    setupNodeEvents(on, config) {
       // implement node event listeners here
      allureWriter(on, config);
            return config;
     
    },
  video: false,
  screenshotOnRunFailure: false
  },
});
