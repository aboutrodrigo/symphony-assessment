import ApiPage from '../objects/ApiPage';

describe('API Test', () => {
  beforeEach(() => {
    ApiPage.visit();
  });

  it('should verify the number of objects with category "Authentication & Authorization"', () => {
    ApiPage.getResponse().then((response) => {
      expect(response.body).to.have.property('count').to.be.a('number');

      const category = 'Authentication & Authorization';
      const objectsWithCategory = ApiPage.getObjectsWithCategory(response, category);

      const expectedCount = objectsWithCategory.length;

      expect(expectedCount).to.equal(7);

      // Print objects to the terminal
        // Print objects to the Cypress Test Runner's console
        console.log(`Objects with category "${category}":`);
        objectsWithCategory.forEach((obj, index) => {
          console.log(`Object ${index + 1}:`, JSON.stringify(obj, null, 2));
        });

      // Log objects to the Cypress Test Runner's console
      Cypress.log({
        name: 'Objects with category',
        message: `Found ${expectedCount} objects with category "${category}"`,
        consoleProps: () => {
          return {
            'Category': category,
            'Objects': objectsWithCategory
          };
        }
      });
    });
  });
});
