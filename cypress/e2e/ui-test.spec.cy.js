import LoginPage from '../objects/LoginObject';
import InventoryPage from '../objects/InventoryObject';
import data from '../fixtures/login.json';

describe('UI Test', () => {
  it('should verify item sorting by name', () => {
    LoginPage.visit();
    LoginPage.login(data.username, data.password);

    // Verify items are sorted by Name (A -> Z)
    InventoryPage.sortByAscendingOrder();
    InventoryPage.getInventoryItemNames().then(($items) => {
      const names = Array.from($items).map((item) => item.innerText);
      const sortedNames = [...names].sort();
      expect(names).to.deep.equal(sortedNames);
    });

    // Change sorting to Name (Z -> A)
    InventoryPage.sortByDescendingOrder();
    InventoryPage.getInventoryItemNames().then(($items) => {
      const names = Array.from($items).map((item) => item.innerText);
      const sortedNames = [...names].sort().reverse();
      expect(names).to.deep.equal(sortedNames);
    });
  });
});
