class ApiPage {
  visit() {
    cy.request({
      method: 'GET',
      url: 'https://api.publicapis.org/entries',
      failOnStatusCode: false
    }).as('response');
  }

  getResponse() {
    return cy.get('@response');
  }

  getObjectsWithCategory(response, category) {
    const responseBody = response.body;
    const objectsWithCategory = responseBody.entries.filter((obj) => obj.Category === category);
    return objectsWithCategory;
  }
}

export default new ApiPage();
