class InventoryPage {
    sortByAscendingOrder() {
      cy.get('.product_sort_container').select('az');
    }
  
    sortByDescendingOrder() {
      cy.get('.product_sort_container').select('za');
    }
  
    getInventoryItemNames() {
      return cy.get('.inventory_item_name');
    }
  }
  
  export default new InventoryPage();
  